package org.siu.casa.casaforum.payload;


import lombok.Data;

@Data
public class UserSummary {
	private String username;
	private String role;
	private String email;
	private String firstName;
	private String lastName;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public UserSummary(String username, String role, String email, String firstName, String lastName) {
		super();
		this.username = username;
		this.role = role;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return "UserSummary [username=" + username + ", role=" + role + ", email=" + email + ", firstName=" + firstName
				+ ", lastName=" + lastName + "]";
	}
	public UserSummary() {
		super();
	}

}
