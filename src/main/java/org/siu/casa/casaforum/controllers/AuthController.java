package org.siu.casa.casaforum.controllers;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.siu.casa.casaforum.exception.AppException;
import org.siu.casa.casaforum.model.Course;
import org.siu.casa.casaforum.model.Role;
import org.siu.casa.casaforum.model.RoleName;
import org.siu.casa.casaforum.model.User;
import org.siu.casa.casaforum.payload.ApiResponse;
import org.siu.casa.casaforum.payload.JwtAuthenticationResponse;
import org.siu.casa.casaforum.payload.LoginRequest;
import org.siu.casa.casaforum.payload.SignUpRequest;
import org.siu.casa.casaforum.repository.CourseRepository;
import org.siu.casa.casaforum.repository.RoleRepository;
import org.siu.casa.casaforum.repository.UserRepository;
import org.siu.casa.casaforum.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/auth")
@Api(value="Authentication Endpoints")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	JwtTokenProvider tokenProvider;
	
	@PostMapping("/signin")
	@ApiOperation(value = "Signin Using username/email and password", response = ResponseEntity.class)
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if(userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "Username is already taken"), 
							HttpStatus.BAD_REQUEST);
			
		}
		
		if(userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
		}
		
		User user = new User(signUpRequest.getFirstName(), 
							signUpRequest.getLastName(), 
							signUpRequest.getEmail(),
							signUpRequest.getPassword(), 
							signUpRequest.getPhoneNumber(),
							signUpRequest.getUsername());
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		Role userRole = roleRepository.findByName(RoleName.ROLE_STUDENT)
							.orElseThrow(() -> new AppException("User role not set"));
		user.setRoles(Collections.singleton(userRole));
		Course course = courseRepository.findById(1L).orElseThrow(
					() -> new AppException("Course not found")
				);
		user.setCourses(Collections.singleton(course));
		User result = userRepository.save(user);
		
		URI location = ServletUriComponentsBuilder
						.fromCurrentContextPath().path("/api/users/{username}")
						.buildAndExpand(result.getUsername()).toUri();
							
		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
}
