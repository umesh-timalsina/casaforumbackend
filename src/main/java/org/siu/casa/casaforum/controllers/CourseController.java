package org.siu.casa.casaforum.controllers;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import org.modelmapper.ModelMapper;
import org.siu.casa.casaforum.dto.CourseDTO;
import org.siu.casa.casaforum.model.Course;
import org.siu.casa.casaforum.model.User;
import org.siu.casa.casaforum.payload.UserSummary;
import org.siu.casa.casaforum.repository.UserRepository;
import org.siu.casa.casaforum.security.CurrentUser;
import org.siu.casa.casaforum.security.UserPrincipal;
import org.siu.casa.casaforum.service.impl.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/course")
@Api(value="Course Endpoints")
public class CourseController {
	@Autowired
	private CourseService courseService;
	
	@Autowired 
	UserRepository userRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	@GetMapping("/getUserInfo")
	@PreAuthorize("hasRole('ROLE_STUDENT')")
	@ApiOperation("Get Information about the current user")
	public ResponseEntity<UserSummary> getCurrentUserSummary(@CurrentUser 
							UserPrincipal currentUser){
		Long id = currentUser.getId();
		User user = userRepository
						.findById(id).orElseThrow(
						() -> new UsernameNotFoundException("User with id not found"));
		
		List<String> roles = user.getRoles()
								.stream().map((role) -> role.getName().toString())
										.collect(Collectors.toList());
		String userRole = String.join("|", roles);
		UserSummary userSummary = new UserSummary(currentUser.getUsername(),
									userRole,
									currentUser.getEmail(),
									currentUser.getFirstName(), 
									currentUser.getLastName());
		return new ResponseEntity<UserSummary>(userSummary, HttpStatus.OK);
	}
	
	
	@GetMapping
	@PreAuthorize("hasRole('ROLE_STUDENT')")
	@ApiOperation("Get current courses for the currently logged in user")
	public ResponseEntity<List<CourseDTO>> getCourses(@CurrentUser UserPrincipal currentUser) {
		Long id = currentUser.getId();
		Set<Course> courses = courseService.getCourseListForLoggedInUser(id);
		List<CourseDTO> coursesDTO = courses.stream().map(
							(course) -> convertToCourseDTO(course))
							.collect(Collectors.toList());
		return new ResponseEntity<List<CourseDTO>>(coursesDTO, HttpStatus.OK);
	}
	
//	@GetMapping("/{id}")
//	@ApiOperation("Get the details of the course the user clicks")
//	public ResponseEntity<> getCourseDetail(@PathParam(value = "id") Long id){
//		
//	}
	
	private CourseDTO convertToCourseDTO(Course course) {
		CourseDTO courseDTO = modelMapper.map(course, CourseDTO.class);
		return courseDTO;
	}
}
