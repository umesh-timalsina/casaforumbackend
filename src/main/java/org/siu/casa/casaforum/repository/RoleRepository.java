package org.siu.casa.casaforum.repository;

import java.util.Optional;

import org.siu.casa.casaforum.model.Role;
import org.siu.casa.casaforum.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleName name);
}
