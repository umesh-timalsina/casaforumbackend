package org.siu.casa.casaforum.repository;

import org.siu.casa.casaforum.model.CourseAnnouncements;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseAnnouncementsRepository extends JpaRepository<CourseAnnouncements, Long> {
	
}
