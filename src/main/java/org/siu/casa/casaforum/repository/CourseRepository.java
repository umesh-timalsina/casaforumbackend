package org.siu.casa.casaforum.repository;

import java.util.List;

import org.siu.casa.casaforum.model.Course;
import org.siu.casa.casaforum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
}
