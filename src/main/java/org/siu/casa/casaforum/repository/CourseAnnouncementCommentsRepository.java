package org.siu.casa.casaforum.repository;

import org.siu.casa.casaforum.model.CourseAnnouncementComments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseAnnouncementCommentsRepository  extends JpaRepository<CourseAnnouncementComments, Long>{

}
