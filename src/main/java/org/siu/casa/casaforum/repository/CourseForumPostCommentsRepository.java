package org.siu.casa.casaforum.repository;

import org.siu.casa.casaforum.model.CourseForumPostComments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseForumPostCommentsRepository extends JpaRepository<CourseForumPostComments, Long> {

}
