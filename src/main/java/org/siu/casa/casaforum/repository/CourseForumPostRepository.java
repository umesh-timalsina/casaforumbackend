package org.siu.casa.casaforum.repository;

import org.siu.casa.casaforum.model.CourseForumPost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseForumPostRepository extends JpaRepository<CourseForumPost, Long>{

}
