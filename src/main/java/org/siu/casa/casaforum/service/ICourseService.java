package org.siu.casa.casaforum.service;

import java.util.Set;

import org.siu.casa.casaforum.model.Course;
import org.springframework.stereotype.Service;

@Service
public interface ICourseService {
	public Set<Course> getCourseListForLoggedInUser(Long userId);
}
