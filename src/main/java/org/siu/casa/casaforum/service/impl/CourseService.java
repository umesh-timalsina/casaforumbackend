package org.siu.casa.casaforum.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.siu.casa.casaforum.exception.AppException;
import org.siu.casa.casaforum.model.Course;
import org.siu.casa.casaforum.model.CourseAnnouncements;
import org.siu.casa.casaforum.model.User;
import org.siu.casa.casaforum.repository.CourseRepository;
import org.siu.casa.casaforum.repository.UserRepository;
import org.siu.casa.casaforum.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CourseService implements ICourseService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired 
	private CourseRepository courseRepository;
	
	@Override
	public Set<Course> getCourseListForLoggedInUser(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(
				() -> new AppException(String.format("User with id %ld not found.", userId)));
		
		
		return Collections.unmodifiableSet(user.getCourses());
	}
	

}
