package org.siu.casa.casaforum.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.siu.casa.casaforum.model.audit.DateAudit;


@Entity
@Table(name="course_announcements")
public class CourseAnnouncements extends DateAudit{
	
	private static final long serialVersionUID = -6724459856912349246L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="announcement_id")
	private Long id;
	
	@Column(name="announcement_title")
	private String title;
	
	@Column(name="announcement_content")
	private String content;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="course_id", nullable=false)
	private Course course;
	
	@OneToMany(mappedBy="courseAnnouncements")
	private Set<CourseAnnouncementComments> comments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	
}
