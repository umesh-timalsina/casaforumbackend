package org.siu.casa.casaforum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.siu.casa.casaforum.model.audit.DateAudit;

import lombok.Data;

@Data
@Entity
@Table(name="course_forum_post_comments")
public class CourseForumPostComments extends DateAudit{
	
	private static final long serialVersionUID = -8190374442367576849L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_forum_comment_id")
	private Long id;
	
	@Column(name="comment")
	private String comment;
	@ManyToOne
	@JoinColumn(name="course_forum_post_id", nullable=false)
	private CourseForumPost fourmPost;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;
}