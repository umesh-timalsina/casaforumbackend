package org.siu.casa.casaforum.model;

public enum RoleName {
	ROLE_INSTRUCTOR,
	ROLE_STUDENT,
	ROLE_TA
}
