package org.siu.casa.casaforum.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;
import org.siu.casa.casaforum.model.audit.DateAudit;

@Entity
@Table(name="users", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"username"}),
		@UniqueConstraint(columnNames = {"email"})
})
public class User extends DateAudit{

	private static final long serialVersionUID = 15019443211477323L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	private Long id;
	
	@NotBlank
	@Size(max = 40)
	@Column(name="first_name")
	private String firstName;
	
	@NotBlank
	@Size(max = 40)
	@Column(name="last_name")
	private String lastName;
	
	@NaturalId
	@Size(max = 40)
	@Email
	@NotBlank
	@Column(name="email")
	private String email;
	
	@NotBlank
	@Size(max = 100)
	@Column(name="password")
	private String password;
	
	@NotBlank
	@Size(max=20)
	@Column(name="phonenumber")
	private String phoneNumber;
	
	@NotBlank
	@Size(max=20)
	@Column(name="username")
	private String username;
	
	@ManyToMany(fetch= FetchType.LAZY)
	@JoinTable(name = "user_roles", 
			   joinColumns = @JoinColumn(name="user_id"),
			   inverseJoinColumns = @JoinColumn(name="role_id"))
	private Set<Role> roles = new HashSet<>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="enrollment",
				joinColumns = @JoinColumn(name="user_id"),
				inverseJoinColumns = @JoinColumn(name="course_id"))
	private Set<Course> courses = new HashSet<>();
	

	@OneToMany(mappedBy="user")
	private Set<CourseAnnouncements> announcements = new HashSet<>();
	
	public User() {
	}
	
	public User(String firstName, String lastName, String email, String password, String phoneNumber, String username) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.username = username;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	public Set<CourseAnnouncements> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(Set<CourseAnnouncements> announcements) {
		this.announcements = announcements;
	}
	
	
}
