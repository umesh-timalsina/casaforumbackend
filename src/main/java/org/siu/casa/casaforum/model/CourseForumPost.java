package org.siu.casa.casaforum.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.siu.casa.casaforum.model.audit.DateAudit;

import lombok.Data;

@Entity
@Table(name="course_forum_post")
@Data
public class CourseForumPost extends DateAudit{	
	private static final long serialVersionUID = -6571700116525232202L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_forum_post_id")
	private Long id;
	
	@Column(name="post_title")
	private String postTitle;
	
	@Column(name="post_body")
	private String postBody;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	@ManyToOne
	@JoinColumn(name="course_id")
	private Course course;

	@OneToMany(mappedBy="fourmPost")
	private Set<CourseForumPostComments> comments = new HashSet<>();
}
