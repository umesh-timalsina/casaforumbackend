package org.siu.casa.casaforum.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.siu.casa.casaforum.model.audit.DateAudit;

import lombok.Data;


@Entity
@Table(name="course",
	uniqueConstraints = {@UniqueConstraint(columnNames = {"course_id"}),
						 @UniqueConstraint(columnNames = {"course_code"})
	})
public class Course extends DateAudit{

	private static final long serialVersionUID = 7361555217840837182L;
	
	@Column(name="course_id")
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="course_code")
	@Size(min=5, max=10)
	private String courseCode;
	
	@Column(name="course_name")
	@Size(min=5, max=20)
	private String courseName;
	
	@Size(min=5, max=2000)
	@Column(name="course_description")
	private String courseDescription;
	
	@Size(min=5, max=10)
	@Column(name="course_term")
	private String courseTerm;
	
	@OneToMany(mappedBy="course")
	private Set<CourseAnnouncements> announcements = new HashSet<>();
	
	@OneToMany(mappedBy="course")
	private Set<CourseForumPost> forumPosts = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public String getCourseTerm() {
		return courseTerm;
	}

	public void setCourseTerm(String courseTerm) {
		this.courseTerm = courseTerm;
	}

	public Set<CourseAnnouncements> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(Set<CourseAnnouncements> announcements) {
		this.announcements = announcements;
	}

	public Set<CourseForumPost> getForumPosts() {
		return forumPosts;
	}

	public void setForumPosts(Set<CourseForumPost> forumPosts) {
		this.forumPosts = forumPosts;
	}
}
