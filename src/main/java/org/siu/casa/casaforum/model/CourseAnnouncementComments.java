package org.siu.casa.casaforum.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.siu.casa.casaforum.model.audit.DateAudit;

import lombok.Data;

@Data
@Table(name="course_announcement_comments")
@Entity
public class CourseAnnouncementComments extends DateAudit{
	/**
	 * Entity class to represent course announcements comments
	 */
	private static final long serialVersionUID = 2814465208319990003L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="course_announcement_comment_id")
	private Long id;
	
	@Column(name="comment")
	private String comment;
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="course_id", nullable=false)
	private CourseAnnouncements courseAnnouncements;

}
