package org.siu.casa.casaforum.dto;

import lombok.Data;

@Data
public class CourseDTO {
	private Long id;
	private String courseCode;
	private String courseName;
	private String courseDescription;
	private String courseTerm;
	
}
